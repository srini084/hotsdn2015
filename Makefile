REPORT=paper
TECHREP=paper_tr
LATEX=pdflatex
BIBTEX=bibtex --min-crossrefs=1000
REF1=ref
REF2=rfc

TEX = $(wildcard *.tex)
CLS = $(wildcard *.cls)
SRCS = $(TEX)
REFS=$(REF1).bib $(REF2).bib

all: pdf
#.tex.dvi:
#       $(LATEX) $*.tex

#$(REPORT).dvi: figures $(SRCS) $(CLS) paper.bib
$(REPORT).dvi: $(SRCS) $(CLS) paper.bib
	$(LATEX) $(REPORT)
	$(BIBTEX) $(REPORT)
	perl -pi -e "s/%\s+//" paper.bbl
	$(LATEX) $(REPORT)
	$(LATEX) $(REPORT)

#$(REPORT).ps: $(REPORT).dvi figures
$(REPORT).ps: $(REPORT).dvi 
	dvips -Pcmz -t letter $(REPORT).dvi -o $(REPORT).ps
#	dvips -Ppdf -G0 -t letter $(REPORT).dvi -o $(REPORT).ps



#not relevant here
#figures:
#	cd figures; make

view: $(REPORT).dvi
	xdvi $(REPORT).dvi

print: $(REPORT).dvi
	dvips $(REPORT).dvi

pdf: $(SRCS) $(CLS) paper.bib
	# ps2pdf14 $< $(REPORT).pdf
	#ps2pdf14 -dPDFSETTINGS=/prepress -dSubsetFonts=true -dEmbedAllFonts=true $(REPORT).ps $(REPORT).pdf
	$(LATEX) $(REPORT)
	$(BIBTEX) $(REPORT)
	perl -pi -e "s/%\s+//" paper.bbl
	$(LATEX) $(REPORT)
	$(LATEX) $(REPORT)

printer: $(REPORT).ps
	lpr $(REPORT).ps

web: pdf
	scp $(REPORT).pdf davis.cc.gt.atl.ga.us:/home/avr/public_html/paper/

tidy:
	rm -f *.dvi *.aux *.log *.blg *.bbl

clean:
	rm -f *~ *.dvi *.aux *.log *.blg *.bbl $(REPORT).ps $(REPORT).pdf

