\section{\MakeUppercase{\ourname}}
\label{section:proposal}

In this section, we discuss our proposal for state management to improve
network programmability.

%We first discuss the mechanisms and requirements of revision
%control in Section~\ref{subsection:abstraction}. We then present the
%architecture of {\em \ourname} in Section~\ref{subsection:architecture}. A
%brief outline of the potential challenges from an SDN perspective follows in
%Section~\ref{subsection:challenges}.

\subsection{State Management Abstraction}
\label{subsection:abstraction}

The existence of multiple authors attempting to operate on a shared resource
(network state) dictates the following requirements for concurrent access and
accountability:

\begin {itemize}
\setlength{\itemsep}{-1mm}
\item {\em Author or application tracking}: The system should possess the
ability to exactly identify and report the author that was responsible for
the change of network state.
\item {\em Versioning and change tracking}: Transformation and evolution of
network state can be tracked, providing valuable information that can be
used for planning and provenance reasons.
\item {\em State safety}: Preventing the alteration of
network state that has been committed by a different author brings in
safety that ensures deterministic system behavior. It is, however,
important to allow the same author to alter actions for previously
established match rule.
%turn can guarantee disruption-free operation on new state changes.
%Immutability can also emphasize the erroneous nature of a certain state and
%safeguard the system from making a recurring transition to that particular state.
\item {\em Conflict resolution and rebasing}: The mutability requirement
in statement management brings with it a need to resolve conflicts or
overlaps in flow space to a feasible extent. Rebasing is the process of
merging changes to an extent allowable by a pre-specified policy, based on
the level of conflict.
\item {\em Annotations}: The applications or authors can also include
comments or other metadata to be associated with the change.
\end {itemize}

In the software development world, the above features are commonly
available in a revision control software, such as \texttt {git}
\cite{Git:2015}. By making an analogy between source code and network
state, we envision that adopting a layer of revision control for
managing network state becomes vital to the programmability of the network.

% Commeting out in the interest of space, can add later. If out of scope,
% reconsider adding.
% Git also provide hooks for {\em Continuous integration} (CI) tools, such as
% Jenkins \cite{jenkins}, that perform compilation tests, unit tests and
% integration tests to deem a change as acceptable. Although such a feature is
% desireable even in the context of network state, we reserve it for future
% study and consider it out of scope for this paper.

\subsection{\ourname Architecture}
\label{subsection:architecture}

We architect the abstraction described above in a manner similar to that
used by \texttt {git} by adopting an ``authoritative'' \ourname server 
that keeps up-to-date state that has been committed and programmed onto
the flow tables. In reality, this server and the authoritative copy of
the state it protects can be placed in two locations:

\begin {itemize}
\setlength{\itemsep}{-1mm}
  \item Co-located on the data plane devices: each data plane device can
  host a separate \ourname server and give it direct access to the
  flow table. With this approach, the state and its management are
  distributed. Each controller and its applications will act as
  \ourname clients that conduct transactions with each of these
  servers. If an operator were to make flow state edits on the device,
  they will use a client to achieve that.
  \item Located external to the device: the \ourname server is hosted
  external to the device, in a centralized location.  All readers and
  writers to the flow state will use a \ourname client to access
  it. The flow state in the server is always kept in-sync with the
  flow table in the data plane devices.
\end {itemize}

%From an architectural standpoint of \ourname, a distributed system provides
%optimal coverage for snapshot creation over multiple interfaces to ensure
%network state consistency.
To avoid the overhead on switch CPU and to keep state management simpler to
reconstruct on a network-wide scale, we prefer the latter approach of the
\ourname repository being external to the devices in a centralized
server\footnote {The centralized server can be sufficiently replicated
and prevented from being a weak link in the SDN programming.}.
The implementation of this server becomes even simpler if one were
to host it at a controller instance. For the rest of the paper, we work
with the architecture choice of the \ourname server being co-located
with a controller instance.  

In this approach, all readers (often switch) and
writers (often controller), similar to \texttt {git}'s distributed approach, 
have their own local object database and staging indexes.
%TWe also assume that majority of network state programming takes place through
%the controller. 
Our architecture requires the writers to use a {\em flowmod monitor} module
(running in the background) to monitor and analyze flow-modification messages
received by the SDN/OpenFlow agent, as well as to version network state
information that passes through it. The changes are then pushed to the
\ourname server. The pushed changes are serialized to disk regularly.
Figure~\ref{fig:gitflow-arch} provides an architectural
overview of this process.
When a change is made and committed to the set of past
changes, we create a new ``snapshot'', which is a copy of the
flow state that is versioned and uniquely identified.

When the system initializes, the server sets up the revision control
framework by creating an empty master branch as part of the \ourname server.
Any data plane device that connects to the controller triggers the fork of a
branch from the master's initial state. This new branch is then tagged with the
datapath-id associated with the corresponding device. The branching behavior is
illustrated in Figure~\ref{fig:gitflow-branch}. The device then clones this
branch locally. The staging index and repository local to the device are used to
store flow modifications occuring as a result of programming changes effected
directly on it. The local changesets are, then, pushed to the \ourname 
server on the controller to keep the network state in sync. It is
foreseeable that there can be policies to regulate how frequently state
changes are pushed upstreeam. An instance of
the {\em flowmod monitor} running on the switch handles these operations.

\begin {table}
\centering
%\scriptsize
\caption{Configuration state model}
\label{table:config-state-model}
\begin {tabular}{l|l|l}
\hline
\rowcolor[gray]{.9}
Type & Config data & Example\\
\hline
Timestamp & \{time\} & 1425884076.342\\
\hline
Author & \{app\_uuid\} & ...1681e6b88ec1\\
\hline
Version & \{change\_uuid\} &  ...a79d110c98fb\\
\hline
Table & \{table\_id\} & 5\\
\hline
Flow rule & \{priority, match, & priority: 32768,\\
         & action\} & match: tp\_dst=22,\\
         & & action: drop\\
\hline
Timeout & \{idle, hard\} & idle: 10\\
        & &  hard: 0\\
\hline
Metadata & \{OF config input\} &  vxlan\_remote\_ip= \\
        & & 1.1.1.5
\end {tabular}
\end{table}

\begin{figure}[t]
\centering
\includegraphics[width=1\linewidth,angle=0]{figures/gitflow-branch.eps}
\caption{GitFlow Branching Model}
\label{fig:gitflow-branch}
\vskip -0.5pc
\end{figure}

The flow information and metadata that is important to track changes across
versions is represented as a facile data table in
Table~\ref{table:config-state-model}.

Revision control can be performed at two levels of granularity:
`per-flow' and `flow-table'. Based on the application and operational
environment, it is understandable that an user might pick a different
granularity to perform state management at.
The controller can choose to make available a snapshot at either
granularity across its other instances, or the other instances can
choose to directly ``pull'' from the \ourname repository. This ensures that
all the instances are consistent with the underlying network state.

Although our paper and the Table~\ref{table:config-state-model}
primarily deal with managing flow state, there are other network states
worth preserving, especially the operational state pertaining to
liveness of switch resources. For instance, there are OpenFlow flow
configurations, such as failover group actions, that specify actions
conditional on the liveness of ports (logical and physical). During
troubleshooting or provenance inspection, such actions may be
incorrectly interpreted if the operational state of the ports is not
correlated with the configuration state. To address this need, we add an
additional agent for tracking this operational state and archiving it to
a centralized store alongside the \ourname repository.

\begin{figure}[t]
\centering
\includegraphics[width=1\linewidth,angle=0]{figures/gitflow-arch.eps}
\caption{GitFlow Architecture}
\label{fig:gitflow-arch}
\vskip -0.5pc
\end{figure}

\subsection {Conflict Resolution and Rebasing}
\label{section:rebasing}

During flow state programming, it is common to experience conflict with the
flow space of an existing rule. In the past, SDN implementations
based on OpenFlow relied on external mechanisms or implementations, such as
FortNOX \cite{Porras:2012wn}, FlowVisor \cite{Sherwood:2009wx} or that
used in \cite{Natarajan:2012ku}, to perform
conflict resolution or provide flow space isolation. Recently, the OpenFlow
specification \cite{openflow-spec} included an option to mark a flag called
\texttt {OFPFF\_CHECK\_OVERLAP} on a \texttt {FLOW\_MOD} operation to force the
data plane to verify if the current \texttt {FLOW\_MOD} overlaps with an
existing rule on the match set; in the event of an overlap, the switch must
refuse the addition and respond with an OpenFlow error message. This overlap
check, however, is too restrictive and insufficient when frequent flow state
programming from multiple authors (applications) becomes common.

Revision control and rebasing (locally replaying and merging changes),
as used by \texttt {git}, can be an important tool for programmers. 
In the \ourname architecture, we include the logic proposed in Algorithm~\ref{algo:rebasing}
to attempt local conflict resolution to a feasible extent. The algorithm takes
as input some merging policies, to specify what the action should be when
\ourname detects a conflict and is unable to automatically resolve. 
Our approach relies on building a set of flow-spaces, overlapping
and non-overlapping, for the new flow modification and the existing rule that it
potentially overlaps with. Non-overlapping spaces are accepted right away. The
overlapping space is checked against the current rule a second time to determine
the set relation between the two and the requisite action to be performed.

The approach will need further extension to make the rebasing
author-aware, i.e., only allowing merges based on the author privileges
or based on whether changes are overwriting the same author's past commits.
A \texttt {git}-like approach offers user authentication and privilege tracking
to specify what source code can be modified by which user. We reserve
all author-aware rebasing for future work.

\begin{algorithm}[t]
  \caption{Conflict Resolution and Rebasing}\label{algo:rebasing}
  \begin{algorithmic}[1]
    \Procedure{$resolve\_rebase$}{$dpid,flow\_mods$}
    \ForAll{$flow\_mod \in flow\_mods$}
        %\ForAll{$rule \in committed\_flow\_space$}
            \State $S\gets$ current snapshot of flow space
            \State \textbf{compare} $flow\_mod$ to $S$
            \If{$disjoint(flow\_mod, $S$)$}
                \State accept flow\_mod
            \Else
                \State compute overlap(flow\_mod, S)
                \State $X\gets$ non-overlapping space
                \State $Y\gets$ overlapping space
                \State $Z \subset S, match(Z) = match(Y)$ 
                \State accept $X$
                \If{$(Y, Z)$ is exact\_overlap}
                    \State $action(Y)\gets action(Z)$
                \ElsIf{$Y \supset Z$}
                    \State apply superset merge policy
                \Else
                    \State apply subset merge policy
                \EndIf
            \EndIf
            \State commit flow\_mod
         %\EndFor
    \EndFor
    \EndProcedure
  \end{algorithmic}
\end{algorithm}

%\begin{itemize}
%  \item \emph{gitflow rebase}: Applications can fork their own branches to
%  modify the state, such as the optimizer application in
%  Figure~\ref{fig:gitflow-arch}. Commits can then be rebased onto the device's
%  main branch using \emph{rebase} to obtain an updated, consistent state.
%  \item \emph{gitflow apply}: Using \emph{apply}, a flow modification patch can
%  be applied to update the state in the flow table. \emph{Rebase} can be used
%  internally to maintain state consistency.
%\end{itemize}

%\subsection{Granularity of Revision Control}
%\label{subsection:granularity}

%The effectiveness of any system depends on the demands placed on it
%by its operational environment. We require different levels of
%granularity to deal with multivariate situations. We propose two basic levels of
%granularity, `per-flow' versioning and `flow-table' snapshots.

%We hypothesize that the versioned network state can be abstracted and made
%available on the controller. Northbound applications can utilize current
%network state to make optimal choices. Abstracted network state can potentially
%assist network control programs (running on distributed controllers in a
%wide-area network) in making intelligent routing decisions across the entire
%network. A higher level of intelligence will be available to OpenStack Neutron
%\cite{Quantum:2015} to aid control plane decision making.

%The snapshots of flow tables provides access to the multiple versions of
%the underlying network state. Flow table snapshots can be: (1) \emph{periodic}:
%data plane devices create snapshots of all tables at periodic intervals and
%push them to their respective branches on the controller, or (2)
%\emph{event-driven}: certain events that occur on the data plane, such as link
%or interface failures can trigger snapshot creation. This is to prevent flow
%table data on the controller becoming inconsistent with the actual state of the
%network.

%Some environments are more secure than others and require a finer grain of
%control over revisioning. Per-flow revision control, thus, is requirement
%driven and can be provided in addition to snapshot capabilities.
  
% \subsection{Divergence with the Git Model}
% \label{subsection:differences}
% 
% Branching in \ourname is marginally divergent in nature than the original Git
% model. Although it serves the same purpose, the frequency and nature of
% branching differs here. The probable need for unified network state accumulated
% across all the devices (a single, unified flow table) is quite low, maybe even
% non-existent. As a result, the likelihood of each individual branch emanating
% from the master being merged back is nil. 
% 
% Applications that express the need to utilize exposed network state (and
% subsequently generate flow modifications) can fork ``application'' branches
% from each device's main branch to operate on the flow rules. Post-operation,
% these ``application branches'' can be merged back into the device's main branch
% to provide updated network state information. A device can then pull these
% changes into it's local repository from the controller before committing it to
% the TCAM. A brief illustration of this has been provided as the optimizer
% northbound application in the \ourname architecture in
% Figure~\ref{fig:gitflow-arch}.
% 
% From our discussions, it is clearly discernible that maintaining revision
% control for network state is much like versioning code. We can leverage
% existing approaches such as Git from the system and software design world that
% can be abstracted and adapted to operate on network state. Our contributions in
% this paper are primarily to illustrate that network state can definitely be
% revisioned and there are substantial gains to be achieved in doing so.
