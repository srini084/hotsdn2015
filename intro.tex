\section {Introduction}
\label {section:intro}

% What aspect of SDN is relevant for us and what are the challenges
Software-Defined Networking (SDN) offers an opportunity to both the control and
data planes of today's networks to become programmable and easily evolvable.
The most popular way to realize SDN has been to adopt devices that support
a standardized application programming interface (API), for example OpenFlow
\cite{OpenFlow}, for programming network behavior in the data plane through
external software. This network programmability brings
with it several challenges: migrating to new architectures, interoperating with
legacy networks, coordinating control plane state in a centralized fashion,
supporting different authors to the state, and restricting anomalous or malicious
behaviors. 

While the aspect of multiple co-existing authors editing flow state of a shared
resource has been previously investigated \cite{frenetic, Porras:2012wn,
Sherwood:2009wx, Khurshid:2013er} from the perspective of conflict detection or
resource isolation, the issue of \textit{tracking} all changes to the flow state has
not yet been examined sufficiently. In software development, this process of
managing changes to shared data is referred to as {\em revision
control}\footnote{The popular revision control software \texttt {git} provides
author tracking, versioning and timestamping of changes, immutability of past
alterations, automated conflict resolution, and annotations.}.
We see a need for similar revisioning and state change management rapidly
manifesting in software-defined networks.

% What is our abstraction / proposal
Traditionally, revision control in the networking world has been limited
to managing CLI configurations of data plane devices. The state of the flows
and the control plane, however, is inaccessible. In the context of an
SDN-enabled network, we have access to much more state than was previously
possible. Based on existing software revision control techniques, we propose a
system called \ourname that provides flow state revisioning in the SDN context.
\ourname underlies all flow-level state management that a switch undertakes,
and provides safety at the data plane and better programming discipline in the
control plane.

% Applications
Additionally, our framework can be easily extended for several other purposes:
tracking control plane evolution (similar to past works on AS path evolution
\cite {Dhamdhere:2011em}); providing
network level accountability (similar in motivation to past works on AIP \cite
{aip} or packet provenance \cite {packet-provenance}); preventing flow space
conflicts across different authors (without being as restrictive as the
isolation efforts of FlowVisor \cite {flowvisor} or overlap prevention of
Frenetic \cite{frenetic}); and extracting network state in a form conducive for
automated troubleshooting (beyond methods made possible by header space
analysis \cite{Kazemian:2012jb} and NDB \cite{Handigol:2012ds}). This makes the
\ourname abstraction a vital part of any SDN programming workflow.

In this paper, we investigate flow rule revisioning,
so that SDN-enabled networks can provide a higher level of provenance,
security, ease of programmability and support for multiple applications.
The remainder of this paper is organized as follows. Section
\ref{section:background} describes the model and design goals
flow management in software-defined networks. In Section
\ref{section:proposal}, we present our proposal inspired by the \texttt{git}
version control system. In Section~\ref{section:examples}, we
illustrate applications of our \ourname approach to describe it better.
Section \ref{section:summary} concludes the paper.

