\section {Background}
\label{section:background}

\begin{figure}
	\centering
	\includegraphics[width=1\linewidth, angle=0]{figures/flow-programming-model.png}
	\caption{Programmability model in SDNs.}
	\label{fig:flow-programming-model}
\end{figure}

Software-Defined Networking allows external network applications to
directly access the state of the data plane through well-defined APIs.
The data plane device type (virtual or physical), flow table types
(e.g., VLAN table, MAC table) and size, and supported
feature set (match fields and supported actions) vary depending on the
deployment scenario (e.g., edge-gateways \cite{Natarajan:2013et}, wide-area
networks \cite{Jain:2013:BEG:2534169.2486019}). 
%For example, an SDN-based
%gateway solution is required to process multiple Ethertypes (MPLS, VLAN, IPv4),
%different network protocol traffic (BGP, BFD, ARP, VRRP, etc.), and support
%tenant-specific requirements (QoS, service-level agreements).
Typically, the packet processing functionality is achieved via a
match-action pairs that span a multi-table pipeline. As the flow table
evolves, through route updates, gratuitous ARP replies or new tenants addition,
multiple tables and
their related attributes (e.g., meters attached to each flows) may be
updated or modified by different applications/authors. All these
characteristics have implications on the network state management. 

A typical model of SDN solution is shown in Figure~\ref
{fig:flow-programming-model}. One or more applications, either
directly through the API or through an abstracted intent layer, edit the
configuration state in the data plane, e.g., the flow table. Correspondingly,
the data plane exports to the external applications certain operational state,
e.g., statistics, relevant for the programming logic. We adopt this layered,
decoupled model as our SDN programming model.

Such decoupling of the control and data planes in the context of SDN can be
challenging when it comes to managing network state. Thus, in this paper
we only focus on the flow rule configuration state. Table~\ref{table:mgmt-reqs}
correlates each of SDN's programmability characteristics to a certain state
management requirement. The safety and mutability requirements have been least
investigated in the past, and we adopt it as the main focus of this paper. In
the subsequent sections, we describe our state management system and illustrate
its use through examples.

\begin {table}
\caption{State management requirements}
\label{table:mgmt-reqs}
\centering
%\begin{footnotesize}
\begin {tabular}{l|l}
\hline
\rowcolor[gray]{.9}
Characteristics & State Requirements\\
\hline
Decoupled network control & Consistency\\
\hline
Dynamicity in application & Mutability\\
\hline
Co-existing applications/authors & Safety\\
\hline
Network troubleshooting & Provenance\\
\end {tabular}
%\end{footnotesize}
\end{table}

% Operational networks present a wide set of constraints due to the varied nature
% of devices involved, proprietary or otherwise. A rudimentary toolset comprising
% of \emph{ping}, \emph{traceroute}, \emph{tcpdump} etc., does not make the task
% at hand any easier. Network admins have to rely on these tools, manual packet
% traces and simple network management protocols such as SNMP to troubleshoot
% networks. We can view troubleshooting techniques as either reacting to an
% errant network event or proactively curating network state so as to avoid
% causing downtime.

% Proactive approaches include FortNOX \cite{Porras:2012wn} and
% FlowVisor \cite{Sherwood:2009wx}. The former detects and resolves conflicts
% on the control plane even before changes are effected on the data plane while
% the latter partitions the network into multiple slices (including the flow
% space). These approaches try to address issues that could potentially affect
% network operations.

% Reactive troubleshooting approaches first identify the cause of the
% problem and then tackle remediation. Many reactive approaches have been 
% proposed. \emph{ndb} \cite{Handigol:2012ds} is one such debugger that
% operates on the data plane. \cite{Heller:2013hz} tries to utilize
% the layering in SDN and proposes a more structured troubleshooting approach to
% aid network admins detect and correct problems faster. 

% \emph{OFRewind} \cite{Wundsam:2011bw} provides a good account of why debugging
% and troubleshooting operational networks is a difficult task. It also points
% out how rewind and replay mechanisms can reproduce errant network states and
% datapath issues. 

% There still exists a fundamental, unresolved issue that shares a commonality
% with all these troubleshooting approaches. Networks seldom record or track the
% transformations caused by modifications to the network state. Many of these
% techniques attempt to incorporate program semantics into the network, but there
% are no approaches that investigate how revision control metadata can prove to be
% a solid ally for network troubleshooters.

% In further sections, we illustrate in detail how revision control can be
% leveraged to aid network troubleshooting and optimization in a variety
% of use cases. We will also attempt to show how packet backtraces when combined
% with revision control will help identify how, when and why traffic operation
% does not match operator intent.
