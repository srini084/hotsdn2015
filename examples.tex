\section {\MakeUppercase{\ourname} Use Cases}
\label{section:examples}

To illustrate the value of \ourname, we
discuss the following scenarios where versioning the flow table can be useful
for network operations and explain the practical value of our
abstraction:

\begin{itemize}
\setlength{\itemsep}{-1mm}
  \item Understanding the evolution of network state maintained in the flow
  table.
  \item Tracing and identifying security issues or misconfigurations in the
  network state.
  \item Adopting state extraction for network troubleshooting.
\end{itemize}

\subsection {Tracking Flow Table Evolution}
\label{section:evolution}

%The network state (flows) maintained in the flow table can evolve over time as
%new flow additions (e.g., configuring a new tenant), deletions (e.g., flow
%timeouts), and modifications (e.g., ARP updates) are programmed by the
%controller. 
As discussed above, flow tables evolve over time through additions,
deletions, and modifications. Current switch implementations and controller
applications have limited capabilities to track these changes. Applications are
required to periodically query the network state maintained in the switch
and parse that information to comprehend the updates made to each flow. This
approach is complex and might require state management in application logic.
\ourname can provide basic snapshotting and inspection features to efficiently
track the evolution of the flow table state. Here we discuss some simple
versioning features to highlights its value:

\begin{itemize}
\setlength{\itemsep}{-1mm}
    \item \emph{gitflow commit}: The \emph{commit} feature takes a snapshot of
    the changes made to the flow table from the previous committed state. When
    the controller programs flow modification messages (e.g, either a single or
    bundle of flows), \ourname considers this as a new commit. When flows are
    programmed in the flow table, the committed changes are merged to
    the master copy and recorded as a version of the network state. A commit
    highlights valuable information on changes made to the flow table and eases
    an application or administrator to track the evolution of the flow table.
    \item \emph{gitflow diff}: With \emph{diff}, GitFlow can compare and
    highlight the updates between commits or between the current master state
    and any previously snapshotted version. This helps in comprehending the
    modifications made to individual flows, specific tables and the
    updated actions. 
    \item \emph{gitflow tag}: Tagging is a critical component in GitFlow. Flow
    modifications (commits) can be annotated with tags to carry metadata
    information about the updates. For example, a security update from a
    firewall application can add annotations specific to the application and
    provide descriptions about the updates. 
    \item \emph{gitflow grep}: \emph{grep} searches for the specified input
    pattern (e.g., all entries matching on port 80, or tagged commits) either
    within a single commit or across commits. 
\end{itemize}

%\begin{figure*}[t]
%\centering       
%\includegraphics[width=1\linewidth,angle=0]{figures/multitable-setup.png}
%\caption{Multiple flow tables}
%\label{fig:multitable}
%\end{figure*}

\subsection {Investigating Security Issues}
\label{section:secbreaches}

Multiple interfaces (sets of controllers, side-channel access via SSH logins,
malicious applications with flow-level access) with write access to the flow
table can introduce security issues in the data plane. A security violation
or malicious update of any manner can impact the packet forwarding
functionality, thereby bringing down the network. For example, network
operators can login to the switch and introduce simple flow updates that can
undermine the network policies maintained by the applications. Similarly,
multiple controllers in EQUAL roles can introduce conflicting flow behavior that
can violate the security policies programmed in the switch. Existing security
enforcement kernels are built as an intermediate layer between the controller
and the switches to detect such violations, however, they lack the feature to
track the local changes made in the data plane (e.g., via SSH access). We
exhibit some \ourname features that can complement such security offerings:

\begin{itemize}
\setlength{\itemsep}{-1mm}
    \item \emph{gitflow blame}: The \emph{blame} feature provides information
    about the changes and the entity/user that modified the interested flow
    entries. Additional annotations such as timestamps, impacted tenant
    information, updates (revisions) to individual flows are provided to detail
    the interested changes.
    \item \emph{gitflow bisect}: \emph{bisect} identifies the changes that
    introduced a bug or violation between two versions of the flow table. In
    addition, the options provided with \emph{bisect} can help operators
    backtrack the life of a packet in the data path (e.g., set of tables which
    processed the packet in the multi-table pipeline).
    \item \emph{gitflow reset}: While debugging a flow related security issue,
    an application (or operator) can make use of \emph{reset} feature to revert
    the current flow table to a specific secure state. For example, a
    compromised controller entity can introduce a flow update to re-direct
    data traffic to the master controller (via send to controller action); if
    the operator identifies such a flow update as a security violation, the
    reset command can help reset the flow table state to the last known working
    state and discard all changes made to flow table since the previous
    committed version. 
\end{itemize}

\subsection{Revision Control in Troubleshooting}
\label{subsection:challenges}

Past work on network troubleshooting \cite{Wundsam:2011bw, Heller:2013hz,
Kazemian:2012jb, Handigol:2012ds} highlight how flow space can prove
handy to identify issues. \ourname allows extracting different
snapshots of the flow space, thereby empowering troubleshooting.
% and troubleshooting operational networks is a difficult task
%Questions posed by ndb and Troubleshooting SDNs
This section attempts to address some important questions posed by
Heller et al. \cite{Heller:2013hz} in their analysis for troubleshooting SDNs
and how \ourname can assist in these scenarios.

\begin{enumerate} 
\setlength{\itemsep}{-1mm}
  \item \textbf{How can we integrate program semantics into network
  troubleshooting tools?} The paper discusses network equivalents to {\em gdb,
  valgrind, gprof}, but interestingly, does not talk about revision management.
  Network specific versions of these tools help identify errant network states,
  but revision control can ensure that these situations do not occur again.
  Troubleshooting information can be stored as version metadata that can assist
  in avoiding faulty network configurations from occuring again.
  \item \textbf{How can we integrate troubleshooting information into network
  control programs?} The presence of multiple versions of network state
  actually help troubleshooting tools revert to a working version in the case of
  misconfigurations, failures or such. The use-cases presented in this section
  reinforce this point.
  \item \textbf{What abstractions are useful for troubleshooting?} Exposing
  and abstracting the network state is in itself an interesting proposition.
  Previously, the absence of accountability was a major driver in not exposing
  network state to applications running on the controller. With revision
  control, we have an extra layer of backup to make sure the exposed network
  state is not misused.
\end{enumerate}

As networks grow to be more autonomous and self-healing in nature, automated
troubleshooting tools become a focal point of operations. Flow-level revision
control can potentially aid these tools exercise a more intricate level of
inspection than just investigate changes to the flow tables.

These illustrative use-cases represent the necessity for versioning across
different disciplines such as security and troubleshooting. We believe that
\ourname makes a strong case towards filling an existing gap in network state
management for software-defined networks.
